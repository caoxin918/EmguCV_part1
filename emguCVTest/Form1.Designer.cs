﻿namespace emguCVTest
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.buttonXML = new System.Windows.Forms.Button();
            this.buttonBGR = new System.Windows.Forms.Button();
            this.buttonSetPixels = new System.Windows.Forms.Button();
            this.buttonOpen = new System.Windows.Forms.Button();
            this.CreateButton = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.matrixBox1 = new Emgu.CV.UI.MatrixBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.imageBox5 = new Emgu.CV.UI.ImageBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.imageBox4 = new Emgu.CV.UI.ImageBox();
            this.imageBox3 = new Emgu.CV.UI.ImageBox();
            this.imageBox2 = new Emgu.CV.UI.ImageBox();
            this.button13 = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.imageBox1 = new Emgu.CV.UI.ImageBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button26 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.imageBox10 = new Emgu.CV.UI.ImageBox();
            this.imageBox9 = new Emgu.CV.UI.ImageBox();
            this.imageBox8 = new Emgu.CV.UI.ImageBox();
            this.button23 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button28 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button22 = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.imageBox7 = new Emgu.CV.UI.ImageBox();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.imageBox6 = new Emgu.CV.UI.ImageBox();
            this.button19 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.imageBox16 = new Emgu.CV.UI.ImageBox();
            this.button33 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.imageBox15 = new Emgu.CV.UI.ImageBox();
            this.button32 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.imageBox14 = new Emgu.CV.UI.ImageBox();
            this.imageBox13 = new Emgu.CV.UI.ImageBox();
            this.imageBox12 = new Emgu.CV.UI.ImageBox();
            this.button31 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.imageBox11 = new Emgu.CV.UI.ImageBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button45 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button40 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.imageBox18 = new Emgu.CV.UI.ImageBox();
            this.button37 = new System.Windows.Forms.Button();
            this.histogramBox2 = new Emgu.CV.UI.HistogramBox();
            this.imageBox17 = new Emgu.CV.UI.ImageBox();
            this.button36 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox8)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox7)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox6)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox11)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox17)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1182, 716);
            this.tabControl1.TabIndex = 6;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.buttonXML);
            this.tabPage5.Controls.Add(this.buttonBGR);
            this.tabPage5.Controls.Add(this.buttonSetPixels);
            this.tabPage5.Controls.Add(this.buttonOpen);
            this.tabPage5.Controls.Add(this.CreateButton);
            this.tabPage5.Controls.Add(this.pictureBox2);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1174, 690);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "EmguCV_EssentialChapter5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // buttonXML
            // 
            this.buttonXML.Location = new System.Drawing.Point(331, 119);
            this.buttonXML.Name = "buttonXML";
            this.buttonXML.Size = new System.Drawing.Size(91, 23);
            this.buttonXML.TabIndex = 5;
            this.buttonXML.Text = "XML";
            this.buttonXML.UseVisualStyleBackColor = true;
            // 
            // buttonBGR
            // 
            this.buttonBGR.Location = new System.Drawing.Point(331, 90);
            this.buttonBGR.Name = "buttonBGR";
            this.buttonBGR.Size = new System.Drawing.Size(91, 23);
            this.buttonBGR.TabIndex = 4;
            this.buttonBGR.Text = "B+G+R";
            this.buttonBGR.UseVisualStyleBackColor = true;
            this.buttonBGR.Click += new System.EventHandler(this.buttonBGR_Click);
            // 
            // buttonSetPixels
            // 
            this.buttonSetPixels.Location = new System.Drawing.Point(331, 61);
            this.buttonSetPixels.Name = "buttonSetPixels";
            this.buttonSetPixels.Size = new System.Drawing.Size(91, 23);
            this.buttonSetPixels.TabIndex = 3;
            this.buttonSetPixels.Text = "SetPixels";
            this.buttonSetPixels.UseVisualStyleBackColor = true;
            this.buttonSetPixels.Click += new System.EventHandler(this.buttonSetPixels_Click);
            // 
            // buttonOpen
            // 
            this.buttonOpen.Location = new System.Drawing.Point(331, 32);
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(91, 23);
            this.buttonOpen.TabIndex = 2;
            this.buttonOpen.Text = "Open";
            this.buttonOpen.UseVisualStyleBackColor = true;
            this.buttonOpen.Click += new System.EventHandler(this.buttonOpen_Click);
            // 
            // CreateButton
            // 
            this.CreateButton.Location = new System.Drawing.Point(331, 3);
            this.CreateButton.Name = "CreateButton";
            this.CreateButton.Size = new System.Drawing.Size(91, 23);
            this.CreateButton.TabIndex = 1;
            this.CreateButton.Text = "Create";
            this.CreateButton.UseVisualStyleBackColor = true;
            this.CreateButton.Click += new System.EventHandler(this.CreateButton_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(5, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(320, 240);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.button5);
            this.tabPage6.Controls.Add(this.button4);
            this.tabPage6.Controls.Add(this.matrixBox1);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(1174, 690);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "EmguCV_EssentialChapter6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(311, 12);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 2;
            this.button5.Text = "SetElement";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(202, 12);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 1;
            this.button4.Text = "Create";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // matrixBox1
            // 
            this.matrixBox1.Location = new System.Drawing.Point(3, 3);
            this.matrixBox1.Margin = new System.Windows.Forms.Padding(4);
            this.matrixBox1.Matrix = null;
            this.matrixBox1.Name = "matrixBox1";
            this.matrixBox1.Size = new System.Drawing.Size(383, 227);
            this.matrixBox1.TabIndex = 0;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.button10);
            this.tabPage7.Controls.Add(this.button9);
            this.tabPage7.Controls.Add(this.button8);
            this.tabPage7.Controls.Add(this.button7);
            this.tabPage7.Controls.Add(this.button6);
            this.tabPage7.Controls.Add(this.label2);
            this.tabPage7.Controls.Add(this.label1);
            this.tabPage7.Controls.Add(this.pictureBox4);
            this.tabPage7.Controls.Add(this.pictureBox3);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(1174, 690);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "EmguCV_EssentialChapter7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(337, 192);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 40);
            this.button10.TabIndex = 8;
            this.button10.Text = "ContoursDetect";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(337, 147);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 39);
            this.button9.TabIndex = 7;
            this.button9.Text = "Canny+HoughCircles";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(336, 102);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 39);
            this.button8.TabIndex = 6;
            this.button8.Text = "HoughLinesBinary";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(336, 73);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 5;
            this.button7.Text = "Canny";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(337, 44);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 4;
            this.button6.Text = "Open";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 288);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "Shape";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "Original";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new System.Drawing.Point(5, 303);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(325, 229);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 1;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(5, 44);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(325, 229);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.button13);
            this.tabPage1.Controls.Add(this.pictureBox5);
            this.tabPage1.Controls.Add(this.button12);
            this.tabPage1.Controls.Add(this.button11);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.imageBox1);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1174, 690);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "chapter1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.button17);
            this.panel1.Controls.Add(this.button16);
            this.panel1.Controls.Add(this.button15);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.imageBox5);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.imageBox4);
            this.panel1.Controls.Add(this.imageBox3);
            this.panel1.Controls.Add(this.imageBox2);
            this.panel1.Location = new System.Drawing.Point(641, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(527, 539);
            this.panel1.TabIndex = 17;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(258, 474);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 23);
            this.button17.TabIndex = 13;
            this.button17.Text = "InsertMask";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(177, 475);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 23);
            this.button16.TabIndex = 12;
            this.button16.Text = "Mask2Gray";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(96, 475);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(75, 23);
            this.button15.TabIndex = 11;
            this.button15.Text = "OpenMask";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(15, 475);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 10;
            this.button14.Text = "OpenImage";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(84, 241);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 9;
            this.label7.Text = "MaskGray";
            // 
            // imageBox5
            // 
            this.imageBox5.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox5.Location = new System.Drawing.Point(289, 256);
            this.imageBox5.Name = "imageBox5";
            this.imageBox5.Size = new System.Drawing.Size(231, 212);
            this.imageBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox5.TabIndex = 8;
            this.imageBox5.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(363, 241);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 7;
            this.label6.Text = "MaskInserted";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(378, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 6;
            this.label5.Text = "Mask";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(97, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "Image";
            // 
            // imageBox4
            // 
            this.imageBox4.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox4.Location = new System.Drawing.Point(15, 256);
            this.imageBox4.Name = "imageBox4";
            this.imageBox4.Size = new System.Drawing.Size(231, 212);
            this.imageBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox4.TabIndex = 4;
            this.imageBox4.TabStop = false;
            // 
            // imageBox3
            // 
            this.imageBox3.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox3.Location = new System.Drawing.Point(289, 22);
            this.imageBox3.Name = "imageBox3";
            this.imageBox3.Size = new System.Drawing.Size(231, 212);
            this.imageBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox3.TabIndex = 3;
            this.imageBox3.TabStop = false;
            // 
            // imageBox2
            // 
            this.imageBox2.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox2.Location = new System.Drawing.Point(15, 22);
            this.imageBox2.Name = "imageBox2";
            this.imageBox2.Size = new System.Drawing.Size(231, 212);
            this.imageBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox2.TabIndex = 2;
            this.imageBox2.TabStop = false;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(443, 414);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 16;
            this.button13.Text = "InsertMask";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new System.Drawing.Point(525, 354);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(100, 108);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 15;
            this.pictureBox5.TabStop = false;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(443, 384);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 14;
            this.button12.Text = "MaskImage";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(443, 354);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 23);
            this.button11.TabIndex = 13;
            this.button11.Text = "Section1_4";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(332, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 12);
            this.label3.TabIndex = 12;
            this.label3.Text = "(x,y):Value";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(234, 25);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 11;
            this.button3.Text = "Flip";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(27, 354);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(400, 123);
            this.textBox1.TabIndex = 10;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(318, 82);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(256, 244);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(152, 25);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "LoadImage";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // imageBox1
            // 
            this.imageBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox1.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox1.Location = new System.Drawing.Point(27, 82);
            this.imageBox1.Name = "imageBox1";
            this.imageBox1.Size = new System.Drawing.Size(274, 254);
            this.imageBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox1.TabIndex = 7;
            this.imageBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(27, 25);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Hello World";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1174, 690);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "chapter2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button26);
            this.groupBox3.Controls.Add(this.button25);
            this.groupBox3.Controls.Add(this.button24);
            this.groupBox3.Controls.Add(this.imageBox10);
            this.groupBox3.Controls.Add(this.imageBox9);
            this.groupBox3.Controls.Add(this.imageBox8);
            this.groupBox3.Controls.Add(this.button23);
            this.groupBox3.Location = new System.Drawing.Point(732, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(298, 409);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "2-4图像运算";
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(152, 191);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(75, 23);
            this.button26.TabIndex = 7;
            this.button26.Text = "重映射运算";
            this.button26.UseVisualStyleBackColor = true;
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(71, 191);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(75, 23);
            this.button25.TabIndex = 6;
            this.button25.Text = "叠加运算";
            this.button25.UseVisualStyleBackColor = true;
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(181, 23);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(75, 23);
            this.button24.TabIndex = 5;
            this.button24.Text = "叠加图像";
            this.button24.UseVisualStyleBackColor = true;
            // 
            // imageBox10
            // 
            this.imageBox10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox10.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox10.Location = new System.Drawing.Point(88, 220);
            this.imageBox10.Name = "imageBox10";
            this.imageBox10.Size = new System.Drawing.Size(128, 128);
            this.imageBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox10.TabIndex = 4;
            this.imageBox10.TabStop = false;
            // 
            // imageBox9
            // 
            this.imageBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox9.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox9.Location = new System.Drawing.Point(152, 52);
            this.imageBox9.Name = "imageBox9";
            this.imageBox9.Size = new System.Drawing.Size(128, 128);
            this.imageBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox9.TabIndex = 3;
            this.imageBox9.TabStop = false;
            // 
            // imageBox8
            // 
            this.imageBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox8.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox8.Location = new System.Drawing.Point(6, 52);
            this.imageBox8.Name = "imageBox8";
            this.imageBox8.Size = new System.Drawing.Size(128, 128);
            this.imageBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox8.TabIndex = 2;
            this.imageBox8.TabStop = false;
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(31, 23);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(75, 23);
            this.button23.TabIndex = 0;
            this.button23.Text = "原始图像";
            this.button23.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button28);
            this.groupBox2.Controls.Add(this.button27);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.button22);
            this.groupBox2.Controls.Add(this.numericUpDown1);
            this.groupBox2.Controls.Add(this.imageBox7);
            this.groupBox2.Controls.Add(this.button20);
            this.groupBox2.Controls.Add(this.button21);
            this.groupBox2.Location = new System.Drawing.Point(369, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(357, 409);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "2-3图像减色";
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(205, 320);
            this.button28.Margin = new System.Windows.Forms.Padding(2);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(65, 45);
            this.button28.TabIndex = 7;
            this.button28.Text = "锐化图像使用filter2D函数";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(285, 377);
            this.button27.Margin = new System.Windows.Forms.Padding(2);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(65, 21);
            this.button27.TabIndex = 6;
            this.button27.Text = "锐化图像";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(1, 320);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(199, 47);
            this.textBox2.TabIndex = 5;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(205, 376);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(75, 23);
            this.button22.TabIndex = 4;
            this.button22.Text = "减色运算2";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(82, 377);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(36, 21);
            this.numericUpDown1.TabIndex = 3;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // imageBox7
            // 
            this.imageBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox7.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox7.Location = new System.Drawing.Point(0, 20);
            this.imageBox7.Name = "imageBox7";
            this.imageBox7.Size = new System.Drawing.Size(350, 294);
            this.imageBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox7.TabIndex = 2;
            this.imageBox7.TabStop = false;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(124, 376);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(75, 23);
            this.button20.TabIndex = 1;
            this.button20.Text = "减色运算";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(0, 376);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 23);
            this.button21.TabIndex = 0;
            this.button21.Text = "打开图像";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.imageBox6);
            this.groupBox1.Controls.Add(this.button19);
            this.groupBox1.Controls.Add(this.button18);
            this.groupBox1.Location = new System.Drawing.Point(6, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(357, 409);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "2-2椒盐噪声";
            // 
            // imageBox6
            // 
            this.imageBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox6.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox6.Location = new System.Drawing.Point(0, 20);
            this.imageBox6.Name = "imageBox6";
            this.imageBox6.Size = new System.Drawing.Size(350, 350);
            this.imageBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox6.TabIndex = 2;
            this.imageBox6.TabStop = false;
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(275, 376);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(75, 23);
            this.button19.TabIndex = 1;
            this.button19.Text = "addNoise";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(0, 376);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(75, 23);
            this.button18.TabIndex = 0;
            this.button18.Text = "Open";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Controls.Add(this.button30);
            this.tabPage3.Controls.Add(this.button29);
            this.tabPage3.Controls.Add(this.imageBox11);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1174, 690);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "chapter3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.imageBox16);
            this.groupBox4.Controls.Add(this.button33);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.imageBox15);
            this.groupBox4.Controls.Add(this.button32);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.imageBox14);
            this.groupBox4.Controls.Add(this.imageBox13);
            this.groupBox4.Controls.Add(this.imageBox12);
            this.groupBox4.Controls.Add(this.button31);
            this.groupBox4.Location = new System.Drawing.Point(359, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(733, 379);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "HSV彩色空间";
            // 
            // imageBox16
            // 
            this.imageBox16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox16.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox16.Location = new System.Drawing.Point(274, 11);
            this.imageBox16.Name = "imageBox16";
            this.imageBox16.Size = new System.Drawing.Size(350, 350);
            this.imageBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox16.TabIndex = 7;
            this.imageBox16.TabStop = false;
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(170, 20);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(75, 23);
            this.button33.TabIndex = 16;
            this.button33.Text = "皮肤识别";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(180, 207);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 15;
            this.label11.Text = "统一亮度";
            // 
            // imageBox15
            // 
            this.imageBox15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox15.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox15.Location = new System.Drawing.Point(140, 222);
            this.imageBox15.Name = "imageBox15";
            this.imageBox15.Size = new System.Drawing.Size(128, 128);
            this.imageBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox15.TabIndex = 14;
            this.imageBox15.TabStop = false;
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(87, 20);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(75, 23);
            this.button32.TabIndex = 13;
            this.button32.Text = "统一亮度";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(168, 54);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 12;
            this.label10.Text = "饱和度通道";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(44, 207);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 11;
            this.label9.Text = "色调通道";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(44, 54);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 10;
            this.label8.Text = "亮度通道";
            // 
            // imageBox14
            // 
            this.imageBox14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox14.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox14.Location = new System.Drawing.Point(140, 69);
            this.imageBox14.Name = "imageBox14";
            this.imageBox14.Size = new System.Drawing.Size(128, 128);
            this.imageBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox14.TabIndex = 9;
            this.imageBox14.TabStop = false;
            // 
            // imageBox13
            // 
            this.imageBox13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox13.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox13.Location = new System.Drawing.Point(6, 222);
            this.imageBox13.Name = "imageBox13";
            this.imageBox13.Size = new System.Drawing.Size(128, 128);
            this.imageBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox13.TabIndex = 8;
            this.imageBox13.TabStop = false;
            // 
            // imageBox12
            // 
            this.imageBox12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox12.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox12.Location = new System.Drawing.Point(6, 69);
            this.imageBox12.Name = "imageBox12";
            this.imageBox12.Size = new System.Drawing.Size(128, 128);
            this.imageBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox12.TabIndex = 7;
            this.imageBox12.TabStop = false;
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(6, 20);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(75, 23);
            this.button31.TabIndex = 7;
            this.button31.Text = "HSV分解";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(86, 359);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(82, 23);
            this.button30.TabIndex = 5;
            this.button30.Text = "DetectBlue";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(5, 359);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(75, 23);
            this.button29.TabIndex = 4;
            this.button29.Text = "LoadImage";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // imageBox11
            // 
            this.imageBox11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox11.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox11.Location = new System.Drawing.Point(3, 3);
            this.imageBox11.Name = "imageBox11";
            this.imageBox11.Size = new System.Drawing.Size(350, 350);
            this.imageBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox11.TabIndex = 3;
            this.imageBox11.TabStop = false;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button45);
            this.tabPage4.Controls.Add(this.button44);
            this.tabPage4.Controls.Add(this.button43);
            this.tabPage4.Controls.Add(this.button42);
            this.tabPage4.Controls.Add(this.button41);
            this.tabPage4.Controls.Add(this.groupBox5);
            this.tabPage4.Controls.Add(this.textBox3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1174, 690);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "chapter4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // button45
            // 
            this.button45.Location = new System.Drawing.Point(516, 464);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(127, 52);
            this.button45.TabIndex = 23;
            this.button45.Text = "基于积分图像的自适应阈值分割";
            this.button45.UseVisualStyleBackColor = true;
            this.button45.Click += new System.EventHandler(this.button45_Click);
            // 
            // button44
            // 
            this.button44.Location = new System.Drawing.Point(383, 464);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(127, 52);
            this.button44.TabIndex = 22;
            this.button44.Text = "比较直方图搜索相似图像";
            this.button44.UseVisualStyleBackColor = true;
            this.button44.Click += new System.EventHandler(this.button44_Click);
            // 
            // button43
            // 
            this.button43.Location = new System.Drawing.Point(235, 522);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(127, 23);
            this.button43.TabIndex = 21;
            this.button43.Text = "均值平移查找目标";
            this.button43.UseVisualStyleBackColor = true;
            this.button43.Click += new System.EventHandler(this.button43_Click);
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(235, 493);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(127, 23);
            this.button42.TabIndex = 20;
            this.button42.Text = "反向投影直方图BGR";
            this.button42.UseVisualStyleBackColor = true;
            this.button42.Click += new System.EventHandler(this.button42_Click);
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(235, 464);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(127, 23);
            this.button41.TabIndex = 19;
            this.button41.Text = "反向投影直方图Gray";
            this.button41.UseVisualStyleBackColor = true;
            this.button41.Click += new System.EventHandler(this.button41_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button40);
            this.groupBox5.Controls.Add(this.button39);
            this.groupBox5.Controls.Add(this.button38);
            this.groupBox5.Controls.Add(this.imageBox18);
            this.groupBox5.Controls.Add(this.button37);
            this.groupBox5.Controls.Add(this.histogramBox2);
            this.groupBox5.Controls.Add(this.imageBox17);
            this.groupBox5.Controls.Add(this.button36);
            this.groupBox5.Controls.Add(this.button35);
            this.groupBox5.Controls.Add(this.button34);
            this.groupBox5.Location = new System.Drawing.Point(5, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(819, 455);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "直方图计算";
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(674, 422);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(92, 23);
            this.button40.TabIndex = 18;
            this.button40.Text = "直方图均衡化";
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(587, 422);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(81, 23);
            this.button39.TabIndex = 17;
            this.button39.Text = "伸展直方图";
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(431, 422);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(150, 23);
            this.button38.TabIndex = 16;
            this.button38.Text = "利用查找表反转灰度图像";
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.button38_Click);
            // 
            // imageBox18
            // 
            this.imageBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox18.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox18.Location = new System.Drawing.Point(412, 296);
            this.imageBox18.Name = "imageBox18";
            this.imageBox18.Size = new System.Drawing.Size(401, 124);
            this.imageBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageBox18.TabIndex = 15;
            this.imageBox18.TabStop = false;
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(290, 422);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(135, 23);
            this.button37.TabIndex = 14;
            this.button37.Text = "计算任意图像直方图";
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // histogramBox2
            // 
            this.histogramBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.histogramBox2.Location = new System.Drawing.Point(412, 20);
            this.histogramBox2.Name = "histogramBox2";
            this.histogramBox2.Size = new System.Drawing.Size(401, 271);
            this.histogramBox2.TabIndex = 13;
            // 
            // imageBox17
            // 
            this.imageBox17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox17.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox17.Location = new System.Drawing.Point(6, 20);
            this.imageBox17.Name = "imageBox17";
            this.imageBox17.Size = new System.Drawing.Size(400, 400);
            this.imageBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imageBox17.TabIndex = 8;
            this.imageBox17.TabStop = false;
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(191, 422);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(93, 23);
            this.button36.TabIndex = 2;
            this.button36.Text = "计算BGR直方图";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(88, 422);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(97, 23);
            this.button35.TabIndex = 1;
            this.button35.Text = "计算Gray直方图";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(6, 422);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(75, 23);
            this.button34.TabIndex = 0;
            this.button34.Text = "LoadImage";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(5, 464);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(224, 82);
            this.textBox3.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1197, 731);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox8)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox7)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageBox6)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox11)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox17)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button2;
        private Emgu.CV.UI.ImageBox imageBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button buttonXML;
        private System.Windows.Forms.Button buttonBGR;
        private System.Windows.Forms.Button buttonSetPixels;
        private System.Windows.Forms.Button buttonOpen;
        private System.Windows.Forms.Button CreateButton;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Button button4;
        private Emgu.CV.UI.MatrixBox matrixBox1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label label7;
        private Emgu.CV.UI.ImageBox imageBox5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Emgu.CV.UI.ImageBox imageBox4;
        private Emgu.CV.UI.ImageBox imageBox3;
        private Emgu.CV.UI.ImageBox imageBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button18;
        private Emgu.CV.UI.ImageBox imageBox6;
        private System.Windows.Forms.GroupBox groupBox2;
        private Emgu.CV.UI.ImageBox imageBox7;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button24;
        private Emgu.CV.UI.ImageBox imageBox10;
        private Emgu.CV.UI.ImageBox imageBox9;
        private Emgu.CV.UI.ImageBox imageBox8;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private Emgu.CV.UI.ImageBox imageBox11;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private Emgu.CV.UI.ImageBox imageBox14;
        private Emgu.CV.UI.ImageBox imageBox13;
        private Emgu.CV.UI.ImageBox imageBox12;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Label label11;
        private Emgu.CV.UI.ImageBox imageBox15;
        private System.Windows.Forms.Button button33;
        private Emgu.CV.UI.ImageBox imageBox16;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button34;
        private Emgu.CV.UI.ImageBox imageBox17;
        private System.Windows.Forms.TextBox textBox3;
        private Emgu.CV.UI.HistogramBox histogramBox2;
        private System.Windows.Forms.Button button37;
        private Emgu.CV.UI.ImageBox imageBox18;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button45;
    }
}

